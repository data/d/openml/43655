# OpenML dataset: Football---Expected-Goals-Match-Statistics

https://www.openml.org/d/43655

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
In recent years statisticians and data scientists alike have been trying to come up with new ways to evaluate team performance in Football. Sometimes a result is not a fair reflection on a teams performance, and this is where expected goals come in. 
Expected goals is a relatively new football metric, using quality of passing and goalscoring opportunities to rank a teams performance. Understat.com provides these statistics by using neural networks to approximate this data and I have therefore scraped statistics for matches played between the 2014-15 and 2019-2020 seasons to provide the following dataset.
The Leagues included in this representation are:

English Premier League
La Liga
Bundesliga
Serie A
Ligue 1
Russian Football Premier League

Content
The dataset contains 22 columns, a lot of which will be self explanatory such as date, home team etc. Some of the less common features will be outlined below:
Chance  - the percentage prediction of an outcome based on expected goals.
Expected Goals - the number of goals a team is expected to score based on performance.
Deep - number of passes completed within an estimated 20 yards from goal.
PPDA - number of passes allowed per defensive action in the opposition half.
Expected Points - number of points a team is expected to achieve in this game.
Inspiration
Is the expected goals feature an accurate representation of a teams performance?
How can this feature be improved?
Can we predict the outcome of future games based on previous games?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43655) of an [OpenML dataset](https://www.openml.org/d/43655). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43655/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43655/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43655/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

